const validate = require('mongoose-validator');

const semanticId = {
    type: String,
    required: true,
    maxlength: 128,
    validate: [
        validate({
            validator: 'matches',
            arguments: /^[a-zA-Z][a-zA-Z0-9\-_]*$/,
            message: `semanticId must match regex: ${/^[a-z][a-z0-9\-]*$/}`
        })
    ],
    description: `Id semântico do recurso. Deve conter apenas letras minusculas e hifen.`
};

module.exports = semanticId;
