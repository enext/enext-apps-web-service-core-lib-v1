// TODO: Utilizar o mesmo formato no log do winston e do morgan
// TODO: Adicionar o uuid do request nos logs
const winston = require('winston');
const nconf = require('nconf');

const myFormat = winston.format.printf((info) => {
  return `[${info.timestamp}] [${info.level}]: ${info.message}`;
});

const log = winston.createLogger({
  level: nconf.get('log:level'),
  transports: [
    new winston.transports.Console(),
  ],
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.splat(),
    myFormat,
  ),
});

module.exports = log;
