// Must be replaced:
// 1) __UppercaseResourceNameSingular__ Ex.: OrganizationGroup

const __UppercaseResourceNameSingular__Model = require('../models/__UppercaseResourceNameSingular__Model');
const BaseCrudService = require('enext-labs-apps-web-service-core-lib-v1/services/BaseCrudService');
const log = require('enext-labs-apps-web-service-core-lib-v1/utils/logUtils');
const nconf = require('nconf');

class __UppercaseResourceNameSingular__Service extends BaseCrudService{
    async create(modelData) {
        const modelInstance = new __UppercaseResourceNameSingular__Model(modelData);
        return super.treatMongoDbId(super.toObject(await modelInstance.save()));
    }

    async getById({id, populate}) {
        return super.treatMongoDbId(await __UppercaseResourceNameSingular__Model.findById(id).populate(populate).lean());
    }

    async findOne({q, populate}) {
        return super.treatMongoDbId(await __UppercaseResourceNameSingular__Model.findOne(q).populate(populate).lean());
    }

    async list({page, limit, populate, sort}) {
        let q = {};

        let pageOfDocs = await __UppercaseResourceNameSingular__Model.paginate(q, {
            page,
            limit,
            populate,
            sort: super.sanitizeSort(sort, __UppercaseResourceNameSingular__Model.getSortableAttributes()),
            lean: true
        });
        pageOfDocs.docs = pageOfDocs.docs.map((doc) => {
            delete doc.id;
            return super.treatMongoDbId(doc)
        });
        return pageOfDocs;
    }

    async update({id, resourceData}) {
        let q = {_id: id};
        
        let updatedResource = await super.patchUpdate(
            __UppercaseResourceNameSingular__Model,
            q,
            resourceData,
            __UppercaseResourceNameSingular__Model.getUpdateProtectedAttributes(),
            ''
        );

        return super.treatMongoDbId(super.toObject(updatedResource));
    }

    async delete(id) {
        await __UppercaseResourceNameSingular__Model.deleteOne({
            _id: id
        });
    }
}

const serviceInstance = new __UppercaseResourceNameSingular__Service();

module.exports = serviceInstance;
