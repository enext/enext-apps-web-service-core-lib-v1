module.exports = (getContentFunction) => (req, res, next) => {
  req.body = {...req.body, ...getContentFunction(req)};
  next();
};
