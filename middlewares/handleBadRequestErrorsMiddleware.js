const createError = require('http-errors');
const {validationResult} = require('express-validator');

module.exports = () => (req, res, next) => {
  const errors = validationResult(req);

  if (req.invalidId) {
    next(new createError.NotFound());
  }

  if (!errors.isEmpty()) {
    const err = new createError.BadRequest();
    err.errors = errors.mapped();
    next(err);
  } else {
    next();
  }
};
