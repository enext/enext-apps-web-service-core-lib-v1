
const log = require('../utils/logUtils');

/**
 * Classe base de um controller, sem nenhum método definido. Apenas contém o dicionário que mapeia diferentes ações na array de middlewares correspondentes. 
 * role-acl (https://www.npmjs.com/package/role-acl).
 */
class BaseController {
    constructor() {
        this.middlewaresByAction = {};
    }

    init() {
        
    }

    addMethod(controllerMethod) {
        this.middlewaresByAction[controllerMethod.getType()] = controllerMethod.getAllMiddlewares();
    }

    getMiddlewares(action) {
        if(action in this.middlewaresByAction) {
            return this.middlewaresByAction[action];
        }
        else {
            // TODO: Testar o log de erro
            log.error(`Action ${action} not defined in ${JSON.stringify(this.middlewaresByAction, null, 2)}`);
            process.exit(1);
        }
    }
}

module.exports = BaseController;
