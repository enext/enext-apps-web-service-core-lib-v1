function generateExports({
    preMiddlewares,
    actions,
    posMiddlewares
}) {
    const controllerExports = actions.reduce((controllerExports, action) => {
        controllerExports[action.name] = [
            ...preMiddlewares,
            ...action.preMiddlewares,
            action,
            ...action.posMiddlewares,
            ...posMiddlewares
        ];
        return controllerExports;
    }, {});
    return controllerExports;
}

module.exports = {
    generateExports
};
