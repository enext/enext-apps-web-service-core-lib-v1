#!/usr/bin/env node

/**
 * Module dependencies.
 */
const path = require('path');
const nconf = require('nconf');

const allowedNodeEnvValues = ['local', 'dev', 'staging', 'prod'];
const projRootDir = path.join('../../..');

// Setting nconf
nconf.argv().env('__').required(['NODE_ENV']);
if (allowedNodeEnvValues.includes(nconf.get('NODE_ENV'))) {
  const defaultConfig = require(path.join(projRootDir, 'src', 'configs', 'defaultConfig'));
  let envConfig = {};
  try {
    envConfig = require(path.join(projRootDir, 'src', 'configs', 'envConfig'));
    console.info(`envConfig obtida do json`);
  } catch (err) {
    console.info(`envConfig não definida`);
  }
  nconf.defaults({store: {...defaultConfig, ...envConfig}});
} else {
  throw Error( `NODE_ENV must be defined and belong to ${allowedNodeEnvValues}`);
}

const app = require(path.join(projRootDir, 'app'));
const debug = require('debug')('api:server');
const http = require('http');

/**
 * Get port from environment and store in Express.
 */
const port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 *
 * @param {Object} val Unormalized port.
 *
 * @return {string|number|boolean} Normalized port.
 */
function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 *
 * @param {Error} error
 */
function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ?
        'Pipe ' + port :
        'Port ' + port;
  9000;
  // handle9000 specific listen errors with friendly messages
  switch (error.code) {
    case 'E9000ACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string' ?
        'pipe ' + addr :
        'port ' + addr.port;
  debug('Listening on ' + bind);
}
