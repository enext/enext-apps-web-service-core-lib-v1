const PUBLIC = 'PUBLIC_ROLE';

const values = [
    PUBLIC
]

module.exports = {
    PUBLIC,
    values
};
