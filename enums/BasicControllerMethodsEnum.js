const CREATE = 'CREATE';
const GET = 'GET';
const LIST = 'LIST';
const UPDATE = 'UPDATE';
const DELETE = 'DELETE';
const SYNC_CALL = 'SYNC_CALL';
const ASYNC_CALL = 'ASYNC_CALL';

// TODO: Otimizar para não precisar definir values assim
const values = [
    CREATE,
    GET,
    LIST,
    UPDATE,
    DELETE,
    SYNC_CALL,
    ASYNC_CALL
];

module.exports = {
    CREATE,
    GET,
    LIST,
    UPDATE,
    DELETE,
    SYNC_CALL,
    ASYNC_CALL,
    values
}
