// TODO: Habilitar jsdoc
/* eslint-disable require-jsdoc */
const BaseCrudService = require('./BaseCrudService');

/**
 * Classe base de um serviço que provê operações CRUD sobre um recurso de organização.
 */
class BaseOrganizationResourceCrudService extends BaseCrudService {
  /**
   * @override
   *
   * @param {Object} params
   *
   * @return {Object} Page of documents.
   */
  async list({page, limit, populate, sort, organizationId}) {
    let q = {};

    if (typeof organizationId === 'string') organizationId = organizationId.split(',');
    if (organizationId) q = {...q, organizationId: {$in: organizationId}};

    const pageOfDocs = await this.Model.paginate(q, {
      page,
      limit,
      populate,
      sort: this._sanitizeSort(sort, this.Model.getSortableAttributes()),
      lean: true,
      collation: {
        locale: 'en',
      },
    });
    pageOfDocs.docs = pageOfDocs.docs.map((doc) => {
      delete doc.id;
      return this._treatMongoDbId(doc);
    });
    return pageOfDocs;
  }
}

module.exports = BaseOrganizationResourceCrudService;
