// Must be replaced:
// 1) __RESOURCE_NAME_SINGULAR__ Ex.: ORGANIZATION_GROUP
// 2) __RESOURCE_NAME_PLURAL__ Ex.: ORGANIZATION_GROUPS
// 3) __SUBRESOURCE_NAME_SINGULAR__ Ex.: GROUP

const RolesEnum = require('../enums/RolesEnum');
const BasicActionsEnum = require('enext-labs-apps-web-service-core-lib-v1/enums/BasicActionsEnum');

const RESOURCE_NAME = '__RESOURCE_NAME_SINGULAR__';

// Create
const CREATE___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE = 'CREATE___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE';
const CREATE___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE = 'CREATE___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE';

// Get & List
const GET_ANY___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE = 'GET_ANY___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE';
const GET_ANY___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE = 'GET_ANY___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE';

// Update
const UPDATE_ANY___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE = 'UPDATE_ANY___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE';
const UPDATE_ANY___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE = 'UPDATE_ANY___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE';

// Delete
const DELETE_ANY___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE = 'DELETE_ANY___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE';
const DELETE_ANY___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE = 'DELETE_ANY___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE';

// Master
const __RESOURCE_NAME_PLURAL___MASTER_ROLE = '__RESOURCE_NAME_PLURAL___MASTER_ROLE';

const resourceGrants = [
  // Create
  {
    role: CREATE___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
    resource: RESOURCE_NAME,
    action: BasicActionsEnum.CREATE,
    attributes: ['*'],
    condition: {
      Fn: 'EQUALS',
      args: {
        requesterOrganizationId: '$.requestedOrganizationId'
      }
    }
  },
  {
    role: CREATE___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE,
    resource: RESOURCE_NAME,
    action: BasicActionsEnum.CREATE,
    attributes: ['*']
  },
  // Get & List
  {
    role: GET_ANY___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
    resource: RESOURCE_NAME,
    action: [BasicActionsEnum.GET, BasicActionsEnum.LIST],
    attributes: ['*'],
    condition: {
      Fn: 'EQUALS',
      args: {
        requesterOrganizationId: '$.requestedOrganizationId',
      }
    }
  },
  {
    role: GET_ANY___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE,
    resource: RESOURCE_NAME,
    action: [BasicActionsEnum.GET, BasicActionsEnum.LIST],
    attributes: ['*']
  },
  // Update
  {
    role: UPDATE_ANY___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
    resource: RESOURCE_NAME,
    action: BasicActionsEnum.UPDATE,
    attributes: ['*'],
    condition: {
      Fn: 'EQUALS',
      args: {
        requesterOrganizationId: '$.requestedOrganizationId'
      }
    }
  },
  {
    role: UPDATE_ANY___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE,
    resource: RESOURCE_NAME,
    action: BasicActionsEnum.UPDATE,
    attributes: ['*']
  },
  // Delete
  {
    role: DELETE_ANY___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
    resource: RESOURCE_NAME,
    action: BasicActionsEnum.DELETE,
    attributes: ['*'],
    condition: {
      Fn: 'EQUALS',
      args: {
        requesterOrganizationId: '$.ownerOrganizationId'
      }
    }
  },
  {
    role: DELETE_ANY___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE,
    resource: RESOURCE_NAME,
    action: [BasicActionsEnum.DELETE],
    attributes: ['*']
  },
  // Master
  {
    role: __RESOURCE_NAME_PLURAL___MASTER_ROLE,
    resource: RESOURCE_NAME,
    action: [BasicActionsEnum.CREATE, BasicActionsEnum.GET, BasicActionsEnum.LIST, BasicActionsEnum.UPDATE, BasicActionsEnum.DELETE],
    attributes: ['*']
  }
];

function applyExtends(ac) {
  ac.extendRole(RolesEnum.ENEXT_ROOT, [
    __RESOURCE_NAME_PLURAL___MASTER_ROLE
  ]);

  ac.extendRole(RolesEnum.ENEXT_ADMIN, [
    CREATE___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE,
    GET_ANY___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE
  ]);

  ac.extendRole(RolesEnum.ENEXT_STAFF, [
    GET_ANY___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE
  ]);

  ac.extendRole(RolesEnum.EXTERNAL_ORGANIZATION_ADMIN, [
    CREATE___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
    GET_ANY___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
    UPDATE_ANY___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE
  ]);

  ac.extendRole(RolesEnum.EXTERNAL_ORGANIZATION_STAFF, [
    GET_ANY___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE
  ]);
}

module.exports = {
  resourceGrants,
  applyExtends,
  CREATE___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
  CREATE___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE,
  GET_ANY___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
  GET_ANY___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE,
  UPDATE_ANY___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
  UPDATE_ANY___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE,
  DELETE_ANY___SUBRESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
  DELETE_ANY___SUBRESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE,
  __RESOURCE_NAME_PLURAL___MASTER_ROLE,
  RESOURCE_NAME
};
