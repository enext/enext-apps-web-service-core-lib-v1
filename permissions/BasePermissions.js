const AccessControl = require('role-acl');

/**
 * Classe base responsável por definir as permissões de acesso das rotas utilizando a lib
 * role-acl (https://www.npmjs.com/package/role-acl).
 */
class BasePermissions {
  /**
   * Método que retorna o nome do recurso a ser protegido.
   */
  getResourceName() {
    throw new Error('Must be implemented');
  }

  /**
   * @private
   * @abstract
   *
   * Método que retorna a lista de grants de acordo com role-acl (https://www.npmjs.com/package/role-acl).
   *
   * @return {Object[]} Lista de grants.
   */
  getGrants() {
    throw new Error('Must be implemented');
  }

  /**
   * @private
   * @abstract
   *
   * Método que retorna a lista de extends de acordo com role-acl (https://www.npmjs.com/package/role-acl).
   *
   * @return {Object[]} Lista de extends.
   */
  getExtends() {
    throw new Error('Must be implemented');
  }

  /**
   * @private
   *
   * Aplica no AccessControl as grants pré-definidas na classe.
   *
   * @param {AccessControl} ac
   */
  applyGrants(ac) {
    this.getGrants().map((grant) => {
      ac.grant(grant);
    });
  }

  /**
   * Para cada role que é adicionado em extends, devemos adicionar todos os roles básicos do recurso (GET_RESOURCE_ROLE, CREATE_RESOURCE_ROLE etc) caso
   * o jwt contenha no campo subroles um dos roles básicos do recurso.
   */
  /**
   * @private
   *
   * Aplica no AccessControl as extensões de role pré-definidas na classe.
   *
   * @param {AccessControl} ac
   */
  applyExtends(ac) {
    this.getExtends().map((extend) => {
      if (!extend.condition) {
        ac.extendRole(extend.role, extend.childrenRoles);
      }
      else {
        ac.extendRole(extend.role, extend.childrenRoles, extend.condition);
      }
    });
  }

  /**
   * Faz todas inicializações do AccessControl baseado nas permissões configuradas nessa classe, de grants e extended roles.
   *
   * @param {AccessControl} ac
   */
  init(ac) {
    this.applyGrants(ac);
    this.applyExtends(ac);
  }
}

module.exports = BasePermissions;
