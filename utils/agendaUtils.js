const Agenda = require('agenda');
const log = require('./logUtils');
const nconf = require('nconf');
const fs = require('fs');
const path = require('path');

let agenda = new Agenda({ db: { address: nconf.get("mongodb:url"), collection: 'agendajobs' } });

function getFiles(dir) {
    const dirents = fs.readdirSync(dir, { withFileTypes: true });
    const files = dirents.map((dirent) => {
        const res = path.resolve(dir, dirent.name);
        return dirent.isDirectory() ? getFiles(res) : res;
    });
    return Array.prototype.concat(...files);
}

if(nconf.get('PROCESS_TYPE') === 'worker') {
    const jobs = getFiles(path.join(__dirname, '..', 'jobs'));
    log.debug('jobs: %o', jobs);

    jobs.map((jobFileFullPath) => {
        const job = require(jobFileFullPath);
        job.init(agenda);
    });

    if (jobs.length) {
        agenda.on('ready', function () {
            agenda.start();
        });
    }
}

function graceful() {
    agenda.stop(function () {
        process.exit(0);
    });
}

process.on('SIGTERM', graceful);
process.on('SIGINT', graceful);

module.exports = agenda;
