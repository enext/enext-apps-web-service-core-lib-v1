// Must be replaced:
// 1) __UppercaseResourceNameSingular__
// 2) __UppercaseResourceNamePlural__
// 3) __lowercaseResourceNameSingular__
// 4) __RESOURCE_NAME_SINGULAR__
// 5) __ResourcePath__

const express = require('express');
const router = express.Router();
const resourceService = require('../services/__lowercaseResourceNameSingular__Service');
const checkJwtMiddleware = require('enext-labs-apps-web-service-core-lib-v1/middlewares/checkJwtMiddleware');
const {check, body, sanitizeQuery} = require('express-validator');
const HttpStatus = require('http-status-codes');
const createError = require('http-errors');
const log = require('enext-labs-apps-web-service-core-lib-v1/utils/logUtils');
const nconf = require('nconf');
const checkPermissionMiddleware = require('enext-labs-apps-web-service-core-lib-v1/middlewares/checkPermissionsMiddleware');
const resourcePermissions = require('../permissions/__lowercaseResourceNameSingular__Permissions');
const BasicActionsEnum = require('enext-labs-apps-web-service-core-lib-v1/enums/BasicActionsEnum');
const injectDefaultQueryParamatersMiddleware = require('enext-labs-apps-web-service-core-lib-v1/middlewares/injectDefaultQueryParamatersMiddleware');
const checkPaginationQueryParametersMiddleware = require('enext-labs-apps-web-service-core-lib-v1/middlewares/checkPaginationQueryParametersMiddleware');
const assertNotBadRequestErrorsMiddleware = require('enext-labs-apps-web-service-core-lib-v1/middlewares/assertNotBadRequestErrorsMiddleware');
const checkResourceMiddleware = require('enext-labs-apps-web-service-core-lib-v1/middlewares/checkResourceMiddleware');
const checkSemanticIdMiddleware = require('enext-labs-apps-web-service-core-lib-v1/middlewares/checkSemanticIdMiddleware');

/**
 * @api {post} __ResourcePath__ create__UppercaseResourceNameSingular__
 * @apiName create__UppercaseResourceNameSingular__
 * @apiVersion 1.0.0
 * @apiGroup __UppercaseResourceNamePlural__
 * @apiPermission CREATE___RESOURCE_NAME_SINGULAR___ROLE
 * 
 * @apiUse AuthorizationHeader
 * 
 * @apiSchema (Request Body) {jsonschema=../auto_generated/json_schemas/__UppercaseResourceNameSingular__RequestSchema.json} apiParam
 * 
 * @apiSchema (Success Response Body) {jsonschema=../auto_generated/json_schemas/__UppercaseResourceNameSingular__ResponseSchema.json} apiSuccess
 * 
 * @apiUse GenericBadRequestErrorResponse
 */
router.post('__ResourcePath__',
    checkJwtMiddleware(),
    checkPermissionMiddleware({
       execute: BasicActionsEnum.CREATE,
       on: resourcePermissions.RESOURCE_NAME
    }),
    assertNotBadRequestErrorsMiddleware(),
    async (req, res, next) => {
        const resourceData = { ...req.body };
        const createdResource = await resourceService.create(resourceData);
        const responseBody = req.user.permission.filter(createdResource);
        res.status(HttpStatus.OK).json(responseBody);
    }
);

/**
 * @api {get} __ResourcePath__/:id get__UppercaseResourceNameSingular__
 * @apiName get__UppercaseResourceNameSingular__
 * @apiVersion 1.0.0
 * @apiGroup __UppercaseResourceNamePlural__
 * @apiPermission GET_MY___RESOURCE_NAME_SINGULAR___ROLE, GET_ANY___RESOURCE_NAME_SINGULAR___ROLE
 * 
 * @apiUse AuthorizationHeader
 * 
 * @apiSchema (Success Response Body) {jsonschema=../auto_generated/json_schemas/__UppercaseResourceNameSingular__ResponseSchema.json} apiSuccess
 * 
 * @apiUse GenericBadRequestErrorResponse
 * @apiUse GenericNotFoundErrorResponse
 */
router.get('__ResourcePath__/:id', 
    checkJwtMiddleware(),
    checkSemanticIdMiddleware([
        {
            paramKey: 'id',
            mapToResource: async (req) => {
                return await resourceService.findOne({
                    q: {
                        semanticId: req.params.id
                    }
                });
            }
        }
    ]),
    checkResourceMiddleware(async (req) => {
        return await resourceService.findOne({ q: {
            _id: req.params.id
        }, populate: req.query.populate });
    }),
    checkPermissionMiddleware({
        execute: BasicActionsEnum.GET,
        on: resourcePermissions.RESOURCE_NAME
    }),
    async (req, res, next) => {
        const responseBody = req.user.permission.filter(req.fetchedResource);
        res.status(HttpStatus.OK).json(responseBody);
    },
);

/**
 * @api {get} __ResourcePath__ list__UppercaseResourceNamePlural__
 * @apiName list__UppercaseResourceNamePlural__
 * @apiVersion 1.0.0
 * @apiGroup __UppercaseResourceNamePlural__
 * @apiPermission GET_MY___RESOURCE_NAME_SINGULAR___ROLE, GET_ANY___RESOURCE_NAME_SINGULAR___ROLE
 * 
 * @apiUse AuthorizationHeader
 * @apiUse PaginationQueryParameters
 * 
 * @apiSchema (Success Response Body) {jsonschema=../auto_generated/json_schemas/__UppercaseResourceNameSingular__PageResponseSchema.json} apiSuccess
 * 
 * @apiUse GenericBadRequestErrorResponse
 */
router.get('__ResourcePath__',
    checkJwtMiddleware(),
    checkPermissionMiddleware({
        execute: BasicActionsEnum.GET,
        on: resourcePermissions.RESOURCE_NAME
    }),
    injectDefaultQueryParamatersMiddleware(nconf.get('defaultListQueryParameters')),
    checkPaginationQueryParametersMiddleware(),
    assertNotBadRequestErrorsMiddleware(),
    async (req, res, next) => {
        const pageOfResources = await resourceService.list({
            ...req.query
        });
        pageOfResources.docs = pageOfResources.docs.map((doc) => req.user.permission.filter(doc));
        res.status(HttpStatus.OK).json(pageOfResources);
    }
);

/**
 * @api {patch} __ResourcePath__/:id update__UppercaseResourceNameSingular__
 * @apiName update__UppercaseResourceNameSingular__
 * @apiVersion 1.0.0
 * @apiGroup __UppercaseResourceNamePlural__
 * @apiPermission UPDATE_MY___RESOURCE_NAME_SINGULAR___ROLE, UPDATE_ANY___RESOURCE_NAME_SINGULAR___ROLE
 * 
 * @apiUse AuthorizationHeader
 * 
 * @apiSchema (Success Response Body) {jsonschema=../auto_generated/json_schemas/__UppercaseResourceNameSingular__ResponseSchema.json} apiSuccess
 * 
 * @apiUse GenericBadRequestErrorResponse
 * @apiUse GenericNotFoundErrorResponse
 */
router.patch('__ResourcePath__/:id', 
    checkJwtMiddleware(),
    checkSemanticIdMiddleware([
        {
            paramKey: 'id',
            mapToResource: async (req) => {
                return await resourceService.findOne({
                    q: {
                        semanticId: req.params.id
                    }
                });
            }
        }
    ]),
    checkResourceMiddleware(async (req) => {
        return await resourceService.findOne({ q: {
            _id: req.params.id
        }, populate: req.query.populate });
    }),
    checkPermissionMiddleware({
        execute: BasicActionsEnum.UPDATE,
        on: resourcePermissions.RESOURCE_NAME
    }),
    assertNotBadRequestErrorsMiddleware(),
    async (req, res, next) => {
        const resourceData = req.body;
        const updatedResource = await resourceService.update({
            id: req.params.id,
            resourceData
        });
        const responseBody = req.user.permission.filter(updatedResource);
        res.status(HttpStatus.OK).json(responseBody);
    }
);

/**
 * @api {delete} __ResourcePath__/:id delete__UppercaseResourceNameSingular__
 * @apiName delete__UppercaseResourceNameSingular__
 * @apiVersion 1.0.0
 * @apiGroup __UppercaseResourceNamePlural__
 * @apiPermission DELETE_MY___RESOURCE_NAME_SINGULAR___ROLE, DELETE_ANY___RESOURCE_NAME_SINGULAR___ROLE
 * 
 * @apiUse AuthorizationHeader
 */
router.delete('__ResourcePath__/:id',
    checkJwtMiddleware(),
    checkSemanticIdMiddleware([
        {
            paramKey: 'id',
            mapToResource: async (req) => {
                return await resourceService.findOne({
                    q: {
                        semanticId: req.params.id
                    }
                });
            }
        }
    ]),
    checkResourceMiddleware(async (req) => {
        return await resourceService.findOne({ q: {
            _id: req.params.id
        }, populate: req.query.populate });
    }),
    checkPermissionMiddleware({
        execute: BasicActionsEnum.DELETE,
        on: resourcePermissions.RESOURCE_NAME
    }),
    async (req, res, next) => {
        await resourceService.delete(req.params.id);
        res.status(HttpStatus.NO_CONTENT).json({});
    }
);

module.exports = router;
