const ac = require('../utils/accessControlUtils');
const createError = require('http-errors');

// Context can be the context or a function that retrieves the context based on the request
module.exports = ({
  execute,
  on,
  context,
}) => async (req, res, next) => {
  let currContext = null;

  // If context is a function, calling it with req
  if (context && typeof context === 'function') {
    currContext = await context(req);
  }

  try {
    const permission = await ac.can(req.user.mainRole).context(currContext).execute(execute).on(on);
    if (permission.granted) {
      req.user.permission = permission;
      if (req.invalidId) {
        return next(new createError.NotFound());
      } else {
        return next();
      }
    } else {
      return next(new createError.Forbidden());
    }
  } catch (err) {
    return next(new createError.Forbidden());
  }
};
