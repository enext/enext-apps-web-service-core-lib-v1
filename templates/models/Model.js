// Must be replaced:
// 1) __UppercaseResourceNameSingular__

const mongoose = require('mongoose');
require('mongoose-schema-jsonschema')(mongoose);
const mongoosePaginate = require('mongoose-paginate-v2');
const mongoosePatchUpdate = require('mongoose-patch-update');
const validate = require('mongoose-validator');
const apiDocUtils = require('enext-labs-apps-web-service-core-lib-v1/utils/apiDocUtils');
const semanticId = require('enext-labs-apps-web-service-core-lib-v1/models/mongoose_types/semanticId');
const log = require('enext-labs-apps-web-service-core-lib-v1/utils/logUtils');

// Schema
const __UppercaseResourceNameSingular__Schema = mongoose.Schema({
    _id: { 
        type: mongoose.Schema.Types.ObjectId, 
        required: true,
        auto: true,
        description: 'Id do recurso'
    },
    semanticId, 
    name: {
        type: String,
        required: false,
        unique: false,
        maxlength: 128,
        description: 'Nome do recurso'
    },
    description: {
        type: String,
        required: false,
        unique: false,
        maxlength: 1024,
        description: 'Descrição do recurso'
    },
}, {
    timestamps: true
});

// Indexes
__UppercaseResourceNameSingular__Schema.index({createdAt: 1});
__UppercaseResourceNameSingular__Schema.index({createdAt: -1});
__UppercaseResourceNameSingular__Schema.index({updatedAt: 1});
__UppercaseResourceNameSingular__Schema.index({updatedAt: -1});
// __UppercaseResourceNameSingular__Schema.index({semanticId: -1}, {unique: true});

// Protected attributes on patch/put
const updateProtectedAttributes = [
    'foo'
];

// Attributes that can be used on sort
const sortableAttributes = [
    'createdAt',
    'updatedAt'
];

// Boilerplate
__UppercaseResourceNameSingular__Schema.statics.getUpdateProtectedAttributes = () => updateProtectedAttributes;
__UppercaseResourceNameSingular__Schema.statics.getSortableAttributes = () => sortableAttributes;
__UppercaseResourceNameSingular__Schema.plugin(mongoosePaginate);
__UppercaseResourceNameSingular__Schema.plugin(mongoosePatchUpdate);
apiDocUtils.writeSchema({
    jsonSchema: __UppercaseResourceNameSingular__Schema.jsonSchema(),
    model: '__UppercaseResourceNameSingular__'
});
apiDocUtils.writeListSort({
    sortableAttributes,
    model: '__UppercaseResourceNameSingular__'
});

const __UppercaseResourceNameSingular__Model = mongoose.model('__UppercaseResourceNameSingular__', __UppercaseResourceNameSingular__Schema);

(async function() {
    try {
        await __UppercaseResourceNameSingular__Model.createIndexes();
    }
    catch(err) {
        log.error(`__UppercaseResourceNameSingular__Model.createIndexes() error`);
        log.error(err.stack);
        process.exit(1);
    }
})();

module.exports = __UppercaseResourceNameSingular__Model;
