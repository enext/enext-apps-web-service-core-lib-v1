# Enext Labs Apps Web Service Core v1

Lib com código base para construção de web services com a stack NodeJS, Express e MongoDB.

## Importanto a lib

Para utilizar essa lib, basta adiciona-la no package.json, na seção depencencies:

```
"dependencies": {
  "web_core": "https://bitbucket.org/enext/enext-labs-apps-web-service-core-lib-v1.git#vx.x.x"
}
```

Não esqueça de sempre selecionar a versão que você quer:

```
"dependencies": {
  "web_core": "https://bitbucket.org/enext/enext-labs-apps-web-service-core-lib-v1.git#v1.0.0"
}
```

## Preparando um release

* Fazer as alterações no CHANGELOG.md
* Alterar a versão no package.json
* Criar o commit e fazer o push/merges/pull request para a master
* Criar uma tag com a versão
```
git tag -a vx.x.x;
```
* Fazer o push da tag
```
git push origin vx.x.x;
```

# TODO's, Releases & Unreleased

[CHANGELOG.md](./CHANGELOG.md)

# License

[LICENSE.md](./LICENSE.md)

