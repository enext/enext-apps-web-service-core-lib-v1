const createError = require('http-errors');

module.exports = () => (req, res, next) => {
  throw new createError.NotFound(`Resource ${req.url} not found`);
};
