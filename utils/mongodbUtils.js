const waitForMongo = require('wait-for-mongodb');
const nconf = require('nconf');
const log = require('./logUtils');
const mongoose = require('mongoose');

/**
 * Aguarda a conexão com o mongodb
 *
 * @param {function} onMongoDbConnectedCallback
 */
function waitForMongoWrapper(onMongoDbConnectedCallback) {
  log.info(`Waiting mongodb connection...`);

  // Conectando no banco de dados do mongo, caso haja algum erro na conexão o processo será finalizado
  waitForMongo(nconf.get('mongodb:url'), {timeout: nconf.get('mongodb.waitForMongoTimeout')}, function(err) {
    if (err) {
      log.error('Mongodb timeout error. Exiting...');
      process.exit(1);
    } else {
      log.info('Mongodb is online');
      mongoose.connect(nconf.get('mongodb:url'), nconf.get('mongodb:options'))
        .then(async () => {
          try {
            await onMongoDbConnectedCallback();
          } catch (err) {
            log.error(err.stack);
            process.exit(1);
          }
        })
        .catch((err) => {
          log.error(JSON.stringify(err, null, 2));
          process.exit(1);
        });
    }
  });
};

module.exports = {
  waitForMongoWrapper,
};
