/**
 * Função que gera todos os valores de uma enum.
 *
 * @return {string[]}
 */
function getValues() {
  return Object.keys(this).filter((val) => val !== 'getValues');
};

module.exports = {
  getValues,
};
