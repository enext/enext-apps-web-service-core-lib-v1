module.exports = (queryParamters) => (req, res, next) => {
  req.query = {...queryParamters, ...req.query};
  next();
};
