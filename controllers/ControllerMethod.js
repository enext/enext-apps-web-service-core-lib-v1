class ControllerAction {
    constructor({
        type,
        preMiddlewares,
        preMethodMiddlewares,
        mainMiddleware,
        posMethodMiddlewares,
        posMiddlewares
    }) {
        this.type = type;
        this.allMiddlewares = [
            ...preMiddlewares,
            ...preMethodMiddlewares,
            mainMiddleware,
            ...posMethodMiddlewares,
            ...posMiddlewares,
        ];
    }

    getType() {
        return this.type;
    }

    getAllMiddlewares() {
        return this.allMiddlewares;
    }
}

module.exports = ControllerAction;
