const WAIT_10_SECONDS_TO_FINISH = 'WAIT_10_SECONDS_TO_FINISH';

const values = [
    WAIT_10_SECONDS_TO_FINISH
];

module.exports = {
    WAIT_10_SECONDS_TO_FINISH,
    values
};
