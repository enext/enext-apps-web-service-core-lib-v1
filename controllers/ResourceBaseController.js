/* eslint-disable require-jsdoc */
// TODO: Implementar o jsdoc
const BaseController = require('./BaseController');
const BasicControllerMethodsEnum = require('../enums/BasicControllerMethodsEnum');
const HttpStatus = require('http-status-codes');
const ControllerMethod = require('./ControllerMethod');
const nconf = require('nconf');
const {param} = require('express-validator');

const checkPaginationQueryParametersMiddleware = require('../middlewares/checkPaginationQueryParametersMiddleware');
const checkPermissionMiddleware = require('../middlewares/checkPermissionsMiddleware');
const checkResourceMiddleware = require('../middlewares/checkResourceMiddleware');
const checkSemanticIdMiddleware = require('../middlewares/checkSemanticIdMiddleware');
const handleBadRequestErrorsMiddleware = require('../middlewares/handleBadRequestErrorsMiddleware');
const injectDefaultQueryParamatersMiddleware = require('../middlewares/injectDefaultQueryParamatersMiddleware');

/**
 * Controller básico contendo todos os métodos CRUD básicos de um recurso.
 * role-acl (https://www.npmjs.com/package/role-acl).
 */
class ResourceBaseController extends BaseController {
  constructor({
    resourceService,
    resourcePermissions,
    permissionContext,
  }) {
    super();

    this.resourceService = resourceService;
    this.resourcePermissions = resourcePermissions;
    this.permissionContext = permissionContext;
  }

  createControllerMethod({
    type,
    preMethodMiddlewares,
    mainMiddleware,
    posMethodMiddlewares,
  }) {
    return new ControllerMethod({
      type,
      preMiddlewares: this.preMiddlewares(),
      preMethodMiddlewares,
      mainMiddleware,
      posMethodMiddlewares,
      posMiddlewares: this.posMiddlewares(),
    });
  }

  init() {
    this.addMethod(this.createControllerMethod({
      type: BasicControllerMethodsEnum.CREATE,
      preMethodMiddlewares: this.createPreMiddlewares(),
      mainMiddleware: this.create(),
      posMethodMiddlewares: this.createPosMiddlewares(),
    }));

    this.addMethod(this.createControllerMethod({
      type: BasicControllerMethodsEnum.GET,
      preMethodMiddlewares: this.getPreMiddlewares(),
      mainMiddleware: this.get(),
      posMethodMiddlewares: this.getPosMiddlewares(),
    }));

    this.addMethod(this.createControllerMethod({
      type: BasicControllerMethodsEnum.LIST,
      preMethodMiddlewares: this.listPreMiddlewares(),
      mainMiddleware: this.list(),
      posMethodMiddlewares: this.listPosMiddlewares(),
    }));

    this.addMethod(this.createControllerMethod({
      type: BasicControllerMethodsEnum.UPDATE,
      preMethodMiddlewares: this.updatePreMiddlewares(),
      mainMiddleware: this.update(),
      posMethodMiddlewares: this.updatePosMiddlewares(),
    }));

    this.addMethod(this.createControllerMethod({
      type: BasicControllerMethodsEnum.DELETE,
      preMethodMiddlewares: this.deletePreMiddlewares(),
      mainMiddleware: this.delete(),
      posMethodMiddlewares: this.deletePosMiddlewares(),
    }));
  }

  preMiddlewares() {
    return [];
  }

  createPreMiddlewares() {
    return [
      checkPermissionMiddleware({
        execute: BasicControllerMethodsEnum.CREATE,
        on: this.resourcePermissions.RESOURCE_NAME,
        context: this.permissionContext(),
      }),
      handleBadRequestErrorsMiddleware(),
    ];
  }
  create() {
    return async (req, res, next) => {
      const toCreateResource = req.body;
      const createdResource = await this.resourceService.create(toCreateResource);
      const responseBody = req.user.permission.filter(createdResource);
      res.status(HttpStatus.OK).json(responseBody);
    };
  }
  createPosMiddlewares() {
    return [];
  }

  getPreMiddlewares() {
    return [
      checkSemanticIdMiddleware([
        {
          paramKey: '_id',
          mapToResource: async (req) => {
            return await this.resourceService.findOne({
              q: {
                semanticId: req.params._id,
              },
            });
          },
        },
      ]),
      checkPermissionMiddleware({
        execute: BasicControllerMethodsEnum.GET,
        on: this.resourcePermissions.RESOURCE_NAME,
        context: this.permissionContext(),
      }),
      param('_id').isMongoId('The path param \'_id\ must be a valid mongo id if you aren\'t using semanticId.'),
      handleBadRequestErrorsMiddleware(),
      checkResourceMiddleware(async (req) => {
        return await this.resourceService.findOne({
          q: {
            _id: req.params._id,
          }, populate: req.query.populate,
        });
      }),
      handleBadRequestErrorsMiddleware(),
    ];
  }
  get() {
    return async (req, res, next) => {
      const responseBody = req.user.permission.filter(req.fetchedResource);
      res.status(HttpStatus.OK).json(responseBody);
    };
  }
  getPosMiddlewares() {
    return [];
  }

  listPreMiddlewares() {
    return [
      checkPermissionMiddleware({
        execute: BasicControllerMethodsEnum.GET,
        on: this.resourcePermissions.RESOURCE_NAME,
        context: this.permissionContext(),
      }),
      injectDefaultQueryParamatersMiddleware(nconf.get('defaultListQueryParameters')),
      checkPaginationQueryParametersMiddleware(),
      handleBadRequestErrorsMiddleware(),
    ];
  }
  list() {
    return async (req, res, next) => {
      const pageOfResources = await this.resourceService.list({
        ...req.query,
      });
      pageOfResources.docs = pageOfResources.docs.map((doc) => req.user.permission.filter(doc));
      res.status(HttpStatus.OK).json(pageOfResources);
    };
  }
  listPosMiddlewares() {
    return [];
  }

  updatePreMiddlewares() {
    return [
      checkSemanticIdMiddleware([
        {
          paramKey: '_id',
          mapToResource: async (req) => {
            return await this.resourceService.findOne({
              q: {
                semanticId: req.params._id,
              },
            });
          },
        },
      ]),
      checkPermissionMiddleware({
        execute: BasicControllerMethodsEnum.UPDATE,
        on: this.resourcePermissions.RESOURCE_NAME,
        context: this.permissionContext(),
      }),
      param('_id').isMongoId('The path param \'_id\ must be a valid mongo id if you aren\'t using semanticId.'),
      handleBadRequestErrorsMiddleware(),
      checkResourceMiddleware(async (req) => {
        return await this.resourceService.findOne({
          q: {
            _id: req.params._id,
          }, populate: req.query.populate,
        });
      }),
      handleBadRequestErrorsMiddleware(),
    ];
  }
  update() {
    return async (req, res, next) => {
      const toUpdateResource = req.body;
      const updatedResource = await this.resourceService.update({
        id: req.params._id,
        toUpdateResource,
      });
      const responseBody = req.user.permission.filter(updatedResource);
      res.status(HttpStatus.OK).json(responseBody);
    };
  }
  updatePosMiddlewares() {
    return [];
  }

  deletePreMiddlewares() {
    return [
      checkSemanticIdMiddleware([
        {
          paramKey: '_id',
          mapToResource: async (req) => {
            return await this.resourceService.findOne({
              q: {
                semanticId: req.params._id,
              },
            });
          },
        },
      ]),
      checkPermissionMiddleware({
        execute: BasicControllerMethodsEnum.DELETE,
        on: this.resourcePermissions.RESOURCE_NAME,
        context: this.permissionContext(),
      }),
      checkResourceMiddleware(async (req) => {
        return await this.resourceService.findOne({
          q: {
            _id: req.params._id,
          }, populate: req.query.populate,
        });
      }),
      handleBadRequestErrorsMiddleware(),
    ];
  }
  delete() {
    return async (req, res, next) => {
      await this.resourceService.delete(req.params._id);
      res.sendStatus(HttpStatus.NO_CONTENT);
    };
  }
  deletePosMiddlewares() {
    return [];
  }

  posMiddlewares() {
    return [];
  }
}

module.exports = ResourceBaseController;
