const {check} = require('express-validator');
const nconf = require('nconf');

module.exports = () => [
    check('page').isInt({ min: nconf.get('pagination:minPage'), max: nconf.get('pagination:maxLimit')}),
    check('limit').isInt({ min: nconf.get('pagination:minLimit'), max: nconf.get('pagination:maxLimit')}),
];
