const httpStatus = require('http-status');
const moment = require('moment');
const log = require('../utils/logUtils');
const nconf = require('nconf');

// TODO: Implementar uma forma de normalizar os erros.
module.exports = () => (err, req, res, next) => {
  if (err && err.name && err.name === 'ValidationError') {
    err.status = 400;
  }
  if (err && err.name && err.name === 'MongoError') {
    err.status = 400;
  }

  const status = err.status || httpStatus.INTERNAL_SERVER_ERROR;
  const statusClass = httpStatus[`${status}_CLASS`];

  if (statusClass === '5xx') {
    log.error('err: %o', err);

    if (nconf.get('NODE_ENV') === 'test') {
      console.error('err: %o', err);
    }
  }

  res.status(status).json({
    status,
    timestamp: moment().toISOString(),
    name: err.name || null,
    code: err.code || null,
    message: err.message.toString() || null,
    errors: err.errors || null,
  });
};
