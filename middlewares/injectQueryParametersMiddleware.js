module.exports = (getQueryParamtersFuncion) => (req, res, next) => {
  req.query = {...req.query, ...getQueryParamtersFuncion(req)};
  next();
};
