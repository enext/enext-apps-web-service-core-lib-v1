const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const mongoosePatchUpdate = require('mongoose-patch-update');
const log = require('./logUtils');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

/**
 * Função que retorna um modelo do mongo a partir de seu schema. Nesse processo são instalados alguns plugins.
 *
 * @param {Object} params
 *
 * @return {Model}
 */
function buildFromSchema({
  Schema,
  modelName,
  updateProtectedAttributes,
  sortableAttributes,
}) {
  Schema.statics.getUpdateProtectedAttributes = () => updateProtectedAttributes;
  Schema.statics.getSortableAttributes = () => sortableAttributes;
  Schema.plugin(mongoosePaginate);
  Schema.plugin(mongoosePatchUpdate);
  Schema.plugin(beautifyUnique);

  const Model = mongoose.model(modelName, Schema);

  (async function() {
    try {
      await Model.createIndexes();
      log.info(`Indexes created for ${modelName}`);
    } catch (err) {
      log.error(`${modelName}.createIndexes() Error`);
      log.error(err.stack);
    }
  })();

  return Model;
}

module.exports = {
  buildFromSchema,
};
