// Must be replaced:
// 1) __RESOURCE_NAME_SINGULAR__ Ex.: ORGANIZATION_GROUP
// 2) __RESOURCE_NAME_PLURAL__ Ex.: ORGANIZATION_GROUPS

const RolesEnum = require('../enums/RolesEnum');
const BasicActionsEnum = require('enext-labs-apps-web-service-core-lib-v1/enums/BasicActionsEnum');

const RESOURCE_NAME = '__RESOURCE_NAME_SINGULAR__';

// Create
const CREATE___RESOURCE_NAME_SINGULAR___ROLE = 'CREATE___RESOURCE_NAME_SINGULAR___ROLE';

// Get & List
const GET_MY___RESOURCE_NAME_SINGULAR___ROLE = 'GET_MY___RESOURCE_NAME_SINGULAR___ROLE';
const GET_ANY___RESOURCE_NAME_SINGULAR___ROLE = 'GET_ANY___RESOURCE_NAME_SINGULAR___ROLE';

// Update
const UPDATE_MY___RESOURCE_NAME_SINGULAR___ROLE = 'UPDATE_MY___RESOURCE_NAME_SINGULAR___ROLE';
const UPDATE_ANY___RESOURCE_NAME_SINGULAR___ROLE = 'UPDATE_ANY___RESOURCE_NAME_SINGULAR___ROLE';

// Delete
const DELETE_MY___RESOURCE_NAME_SINGULAR___ROLE = 'DELETE_MY___RESOURCE_NAME_SINGULAR___ROLE';
const DELETE_ANY___RESOURCE_NAME_SINGULAR___ROLE = 'DELETE_ANY___RESOURCE_NAME_SINGULAR___ROLE';

// Master
const __RESOURCE_NAME_PLURAL___MASTER_ROLE = '__RESOURCE_NAME_PLURAL___MASTER_ROLE';

const resourceGrants = [
  // Create
  {
    role: CREATE___RESOURCE_NAME_SINGULAR___ROLE,
    resource: RESOURCE_NAME,
    action: BasicActionsEnum.CREATE,
    attributes: ['*']
  },
  // Get & List
  {
    role: GET_MY___RESOURCE_NAME_SINGULAR___ROLE,
    resource: RESOURCE_NAME,
    action: [BasicActionsEnum.GET, BasicActionsEnum.LIST],
    attributes: ['*'],
    condition: {
      Fn: 'EQUALS',
      args: {
        requesterId: '$.requestedId'
      }
    }
  },
  {
    role: GET_ANY___RESOURCE_NAME_SINGULAR___ROLE,
    resource: RESOURCE_NAME,
    action: [BasicActionsEnum.GET, BasicActionsEnum.LIST],
    attributes: ['*']
  },
  // Update
  {
    role: UPDATE_MY___RESOURCE_NAME_SINGULAR___ROLE,
    resource: RESOURCE_NAME,
    action: BasicActionsEnum.UPDATE,
    attributes: ['*'],
    condition: {
      Fn: 'EQUALS',
      args: {
        requesterId: '$.requestedId'
      }
    }
  },
  {
    role: UPDATE_ANY___RESOURCE_NAME_SINGULAR___ROLE,
    resource: RESOURCE_NAME,
    action: BasicActionsEnum.UPDATE,
    attributes: ['*']
  },
  // Delete
  {
    role: DELETE_MY___RESOURCE_NAME_SINGULAR___ROLE,
    resource: RESOURCE_NAME,
    action: BasicActionsEnum.DELETE,
    attributes: ['*'],
    condition: {
      Fn: 'EQUALS',
      args: {
        requesterId: '$.requestedId'
      }
    }
  },
  {
    role: DELETE_ANY___RESOURCE_NAME_SINGULAR___ROLE,
    resource: RESOURCE_NAME,
    action: [BasicActionsEnum.DELETE],
    attributes: ['*']
  },
  // Master
  {
    role: __RESOURCE_NAME_PLURAL___MASTER_ROLE,
    resource: RESOURCE_NAME,
    action: ['*'],
    attributes: ['*']
  }
];

function applyExtends(ac) {
  // ac.extendRole(RolesEnum.TEST, [
  //   CREATE___RESOURCE_NAME_SINGULAR___ROLE,
  //   GET_MY___RESOURCE_NAME_SINGULAR___ROLE,
  //   UPDATE_MY___RESOURCE_NAME_SINGULAR___ROLE,
  //   DELETE_MY___RESOURCE_NAME_SINGULAR___ROLE
  // ]);
}

module.exports = {
  resourceGrants,
  applyExtends,
  CREATE___RESOURCE_NAME_SINGULAR___ROLE,
  GET_MY___RESOURCE_NAME_SINGULAR___ROLE,
  GET_ANY___RESOURCE_NAME_SINGULAR___ROLE,
  UPDATE_MY___RESOURCE_NAME_SINGULAR___ROLE,
  UPDATE_ANY___RESOURCE_NAME_SINGULAR___ROLE,
  DELETE_MY___RESOURCE_NAME_SINGULAR___ROLE,
  DELETE_ANY___RESOURCE_NAME_SINGULAR___ROLE,
  __RESOURCE_NAME_PLURAL___MASTER_ROLE,
  RESOURCE_NAME
};
