// TODO: Habilitar jsdoc
/* eslint-disable require-jsdoc */
const cleanDeep = require('clean-deep');

/**
 * Classe base de um serviço que provê operações CRUD.
 */
class BaseCrudService {
  constructor({
    Model,
  }) {
    this.Model = Model;
  }

  _treatMongoDbId(document) {
    if (document) {
      const keys = Object.keys(document);
      keys.map((key) => {
        if (document[key] && document[key]._bsontype && document[key]._bsontype === 'ObjectID') {
          document[key] = document[key].toString();
        }
        if (typeof document[key] === 'object') {
          document[key] = this._treatMongoDbId(document[key]);
        }
      });
    }
    return document;
  }

  _toObject(mongooseModel) {
    return mongooseModel ? mongooseModel.toObject() : null;
  }

  _patchUpdate({
    EntityModel,
    query,
    toUpdateResource,
    protectedKeys,
    selectedKeys,
  }) {
    return new Promise((resolve, reject) => {
      EntityModel.patchUpdate(
        query,
        cleanDeep(toUpdateResource, {
          emptyArrays: false,
          emptyObjects: false,
          emptyStrings: false,
          nullValues: true,
          undefinedValues: true,
        }),
        protectedKeys,
        selectedKeys,
        (err, updatedEntity) => {
          if (err) {
            reject(err);
          } else {
            resolve(updatedEntity);
          }
        },
      );
    });
  }

  _sanitizeSort(originalSort, sortableAttributes) {
    if (!sortableAttributes || sortableAttributes.length === 0) {
      return '';
    }

    let validSort = false;

    sortableAttributes.map((value) => {
      if (originalSort === value || originalSort === '+' + value || originalSort === '-' + value) {
        validSort = true;
      }
    });

    if (validSort) {
      return originalSort;
    } else {
      return '';
    }
  }

  async create(toCreateResource) {
    const modelInstance = new this.Model(toCreateResource);
    return this._treatMongoDbId(this._toObject(await modelInstance.save()));
  }

  async getById({id, populate}) {
    return this._treatMongoDbId(await this.Model.findById(id).populate(populate).lean());
  }

  async findOne({q, populate}) {
    return this._treatMongoDbId(await this.Model.findOne(q).populate(populate).lean());
  }

  async list({page, limit, populate, sort}) {
    const pageOfDocs = await this.Model.paginate({}, {
      page,
      limit,
      populate,
      sort: this._sanitizeSort(sort, this.Model.getSortableAttributes()),
      lean: true,
      collation: {
        locale: 'en',
      },
    });
    pageOfDocs.docs = pageOfDocs.docs.map((doc) => {
      delete doc.id;
      return this._treatMongoDbId(doc);
    });
    return pageOfDocs;
  }

  async update({id, toUpdateResource}) {
    const query = {_id: id};

    const updatedResource = await this._patchUpdate({
      EntityModel: this.Model,
      query,
      toUpdateResource,
      protectedKeys: this.Model.getUpdateProtectedAttributes(),
      selectedKeys: '',
    });

    return this._treatMongoDbId(this._toObject(updatedResource));
  }

  async delete(id) {
    await this.Model.deleteOne({
      _id: id,
    });
  }
}

module.exports = BaseCrudService;
