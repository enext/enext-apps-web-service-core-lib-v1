const BasicRolesEnum = require('../enums/BasicRolesEnum');
const nconf = require('nconf');
const jwt = require('jsonwebtoken');

module.exports = () => (req, res, next) => {
  try {
    const authorizationHeader = req.headers.authorization;
    const accessTokenCandidate = authorizationHeader.split(' ')[1];
    const secretKey = nconf.get('jwt:secretKey');
    const validJwtPayload = jwt.verify(accessTokenCandidate, secretKey);

    req.user = {
      ...validJwtPayload.user,
      isAnonymous: false,
    };
  } catch (err) {
    req.user = {
      mainRole: BasicRolesEnum.PUBLIC,
      isAnonymous: true,
    };
  }

  return next();
};
