module.exports = (getBodyContentFunction) => (req, res, next) => {
  req.body = {...getBodyContentFunction(req), ...req.body};
  next();
};
