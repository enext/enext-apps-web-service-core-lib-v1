const express = require('express');
const router = express.Router();
const log = require('../utils/logUtils');

// TODO: Implement more complex healthcheck that check the database, dependent services etc
router.get('/', async (req, res, next) => {
  res.status(200).json({simpleHealtcheck: 'success'});
});

module.exports = router;
