const validator = require('validator');
const createError = require('http-errors');
const log = require('../utils/logUtils');

module.exports = (arrayOfSemanticIdMapper) => async (req, res, next) => {
  if (req.query.useSemanticId && req.query.useSemanticId === 'true') {
    for (let i = 0; i < arrayOfSemanticIdMapper.length; i++) {
      const paramKey = arrayOfSemanticIdMapper[i].paramKey;
      const mapToResource = arrayOfSemanticIdMapper[i].mapToResource;
      const paramValue = req.params[paramKey];

      if (!paramValue) {
        log.error(`Invalid paramKey ${paramKey} on checkSemanticIdMiddleware. req.params=${checkSemanticIdMiddleware}`);
        return next(new createError.InternalServerError());
      }

      if (!validator.isMongoId(paramValue)) {
        const resourceCandidate = await mapToResource(req);
        if (resourceCandidate && resourceCandidate._id) {
          req.params[paramKey] = resourceCandidate._id;
        } else {
          req.invalidId = true;
          break;
        }
      }
    }
  }
  return next();
};
