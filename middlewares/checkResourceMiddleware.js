const nconf = require('nconf');
const createError = require('http-errors');

module.exports = (retrieveResourceCandidateFunc) => async (req, res, next) => {
  const resourceCandidate = await retrieveResourceCandidateFunc(req);
  if (resourceCandidate) {
    req.fetchedResource = resourceCandidate;
    return next();
  } else {
    return next(new createError.NotFound());
  }
};
