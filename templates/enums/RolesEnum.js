const ROOT = 'ROOT';
const ADMIN = 'ADMIN';
const STAFF = 'STAFF';

const values = [
    ROOT,
    ADMIN,
    STAFF
];

module.exports = {
    ROOT,
    ADMIN,
    STAFF,
    values
};
