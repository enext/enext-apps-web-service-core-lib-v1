# Changelog

Todas as notáveis mudanças desse projeto serão documentadas nesse arquivo.

O formato é baseado em [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) e também é utilizado [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## TODO's

...

## Unreleased

...

## Releases

## v1.1.0 - 2019-11-29

### Added

* Added injectDefaultInBodyMiddleware.js

## v1.0.2 - 2019-11-29

### Fixed

* Not logging mongo credentials.

## v1.0.1 - 2019-11-29

### Added

* bin/www accepting 'local' env.

## v1.0.0 - 2019-11-29

### Added

* Todo código base para a contrução de um API REST utilizando NodeJS, Express e MongoDB (mongoose).
